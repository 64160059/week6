package com.sasipa.week6;

public class RoBotApp {
    public static void main(String[] args) {
        Robot kapong = new Robot("Kapong", 'K', 0, 0);
        kapong.print();
        for (int i = 0; i < 20; i++)
        {
            kapong.right();
            kapong.down();
        }
        kapong.print();

        Robot blue = new Robot("Blue", 'B', 1, 1);
        blue.print();
    }
}
