package com.sasipa.week6;

import javax.swing.plaf.synth.SynthSplitPaneUI;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        BookBank sasipa = new BookBank("Sasipa",50.0); // Constructor
        sasipa.print();
        
        BookBank prayud = new BookBank("Prayuddd",100000.0);
        prayud.print();
        prayud.withdraw(40000.0);
        prayud.print();

        sasipa.deposit(4000.0);
        sasipa.print();

        BookBank prawit = new BookBank("Prawit",1000000.0);
        prawit.print();
        prawit.deposit(2);
        prawit.print();
    }
}
